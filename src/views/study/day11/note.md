---
title: Day11学习笔记
date: 2021/08/03
---

### 原型和原型链

- 每个函数都有一个prototype属性，指向它的实例的原型
- 每一个对象（除了null）都有一个__proto__属性，指向它的原型
- 每个原型都有一个constructor属性指向关联的构造函数
- 读取实例的属性时，如果找不到就会到与之关联的对象原型中去查找，还找不到就找原型的原型，一直向上（原型链）

```javascript
function Person(){

}
var person = new Person()

Person.prototype === person.__proto__

Person ===  Persopn.prototype.constructor

```
原型也是一个对象，既然是对象，就可以用最原始的方式创建
```javascript
var obj = new Object()
obj.name = 'rudy'
```

![原型](./image/prototype4.png)

`Object.prototype.__proto__ === null`

constructor例子
```javascript
function Person(){

}
var person = new Person()
console.log(person.constructor === Person)
```
虽然person实例没有constructor属性，但是它的原型上有，自己查不到就去原型上查找


## 作用域

- javascript采用词法作用域，也叫静态作用域，函数的作用域在函数定义的时候就已经确定了，与之相对是动态作用域，函数的作用域在调用时才确定

## 执行上下文

javascript可执行代码包括：全局代码、函数代码、eval代码

在执行函数的时候会进行准备工作，这个准备工作也叫执行上下文

每个函数都有执行上下文，便于管理，就会有一个执行上下文栈

### 模拟执行上下文栈

假定执行上下文栈是一个数组

`ECStack = []`

1.先执行全局代码，压入一个全局执行上下文栈,只有当整个程序结束，栈才会清空

`ECStack = [globalContext]`

现在遇到下面代码
```javascript
function fun3() {
    console.log('fun3')
}

function fun2() {
    fun3();
}

function fun1() {
    fun2();
}

fun1();
```
执行栈的变化
```javascript

// 执行fun1
ECStack.push(<fun1> functionContext);

// 执行fun2
ECStack.push(<fun2> functionContext);

// 执行fun3
ECStack.push(<fun3> functionContext);

// fun3执行完毕
ECStack.pop();

// fun2执行完毕
ECStack.pop();

// fun1执行完毕
ECStack.pop();

// 执行其他函数，栈底永远存在globalContext
```
分析下面两段代码区别

```javascript
var scope = "global scope";
function checkscope(){
    var scope = "local scope";
    function f(){
        return scope;
    }
    return f();
}
checkscope();


var scope = "global scope";
function checkscope(){
    var scope = "local scope";
    function f(){
        return scope;
    }
    return f;
}
checkscope()();
```

分析执行栈变化,第一段

```
ECStack.push(<checkscope> functionContext);
ECStack.push(<f> functionContext);
ECStack.pop();
ECStack.pop();
```

第二段

```
ECStack.push(<checkscope> functionContext);
ECStack.pop();
ECStack.push(<f> functionContext);
ECStack.pop();
```

### 变量对象

对于每个执行上下文，都有三个重要属性
- 变量对象
- 作用域链
- this

变量对象是与执行上下文相关的数据作用域，存储了在上下文中定义的***变量***和***函数声明***

全局上下文中的变量对象就是全局对象

### 函数上下文

在函数上下文中，我们用活动对象(activation object, AO)来表示变量对象。

活动对象和变量对象其实是一个东西，只是变量对象是规范上的或者说是引擎实现上的，不可在 JavaScript 环境中访问，只有到当进入一个执行上下文中，这个执行上下文的变量对象才会被激活，所以才叫 activation object 呐，而只有被激活的变量对象，也就是活动对象上的各种属性才能被访问。

活动对象是在进入函数上下文时刻被创建的，它通过函数的 arguments 属性初始化。arguments 属性值是 Arguments 对象

### 执行过程

执行上下文的代码分为两个阶段：分析和执行
即：
1. 代码进入执行上下文
2. 代码执行

#### 进入执行上下文

此时代码还未执行，变量对象包括

1. 函数的所有形参
- 由名称和对应值组成的一个变量对象属性被创建
- 没有实参，属性值设为undefined
  
2. 函数声明

- 由名称和对应值（函数对象（function-object））组成一个变量对象属性被创建
- 如果变量对象存在相同名称的属性，则完全替换这个属性

3. 变量声明

- 由名称和对应值（undefined）组成的一个变量对象属性被创建
- 如果变量名称跟已经声明的形式参数或函数相同，则变量声明不会干扰已经存在的这类属性

举例：

```javascript
function foo(a) {
  var b = 2;
  function c() {}
  var d = function() {};

  b = 3;

}

foo(1);
```

在进入执行上下文后，此时的AO（活动对象）是：
```json
AO = {
    arguments:{
        0:1,
        length:1
    },
    a:1,
    b:undefined,
    c:reference to function c(){},
    d:undefined
}
```

执行阶段，AO是

```json
AO = {
    arguments:{
        0:1,
        length:1
    },
    a:1,
    b:3,
    c:reference to function c(){},
    d:reference to FunctionExpression "d"
}
```

1. 全局上下文的变量对象初始化是全局对象

2. 函数上下文的变量对象初始化只包括 Arguments 对象

3. 在进入执行上下文时会给变量对象添加形参、函数声明、变量声明等初始的属性值

4. 在代码执行阶段，会再次修改变量对象的属性值


### 作用域链

当查找变量的时候，会先从当前上下文的变量对象中查找，如果没有找到，就会从父级(词法层面上的父级)执行上下文的变量对象中查找，一直找到全局上下文的变量对象，也就是全局对象。这样由多个执行上下文的变量对象构成的链表就叫做作用域链

函数的作用域在函数定义的时候就确定了，这是因为函数有一个内部属性[[scope]]当函数创建的时候，就会保存所有父变量对象到其中，可以理解为[[scope]]就是所有父变量对象的层级链，但[[scope]]不代表完整的作用域链

举例：

```javascript
function foo() {
    function bar() {
        ...
    }
}
```

函数创建时，各自的`[[scope]]`为

```javascript
foo.[[scope]] = [
    globalContext.VO
]
bar.[[scope]] = [
    fooContext.AO,
    globalContext.VO
]
```
当函数激活时，进入函数上下文，创建AO、VO后，就会将活动对象添加到作用链的前端

Scope = [AO].concat([[scope]])

### 作用域链和变量对象的创建过程

举例

```javascript
var scope = "global scope";
function checkscope(){
    var scope2 = 'local scope';
    return scope2;
}
checkscope();
```

1. checkscope函数被创建，保存作用域链到内部属性[[scope]]

```javascript
checkscope.[[scope]] = [
    globalContext.VO
]

```
2. 执行checkscope函数，创建checkscope函数执行上下文，checkscope函数执行上下文被压入执行上下文栈
   
```javascript
ECStack = [
    checkscopeContext,
    globalContext
]
```
3. 函数并不立即执行，进行准备工作，第一步，复制[[scope]]属性创建作用域链

```javascript
checkscopeContext = {
    Scope:checkscope.[[scope]]
}
```
4. 用arguments创建活动对象，随后初始化活动对象、加入形参、函数声明、变量声明

```javascript
checkscopeContext = {
    AO:{
        arguments:{
            length:0
        },
        scope2:undefined
    }
    Scope:checkscope.[[scope]]
}
```
5. 将活动对象压入checkscope作用域链顶端

```javascript
checkscopeContext = {
    AO:{
        arguments:{
            length:0
        },
        scope2:undefined
    }
    Scope:[AO,[[Scope]]]
}
```
6. 准备工作做完，开始执行函数，随着函数执行，修改AO属性值

```javascript
checkscopeContext = {
    AO:{
        arguments:{
            length:0
        },
        scope2:'local scope'
    }
    Scope:[AO,[[Scope]]]
}
```

7. 查找scope2的值，返回后函数执行完毕，函数上下文从执行上下文中栈中弹出

```javascript
ECStack = [
    globalContext
]
```