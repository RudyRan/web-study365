---
title: Day9学习笔记
date: 2021/07/31
---

## 扁平数据结构转树

### 递归实现

时间复杂度(O(2^n))

```javascript
    let arr = [
        {id: 1, name: '部门1', pid: 0},
        {id: 2, name: '部门2', pid: 1},
        {id: 3, name: '部门3', pid: 1},
        {id: 4, name: '部门4', pid: 3},
        {id: 5, name: '部门5', pid: 4},
    ]

    getChildren(data,result,pid){
        for (const item of data) {
            if (item.pid === pid) {
            const newItem = {...item, children: []};
            result.push(newItem);
            this.getChildren(data, newItem.children, item.id);
            }
        }
    },
    arrayToTree(data,pid){
        const result = []
        this.getChildren(data,result,pid)
        return result
    }
```

### map存储方式

```javascript
    arrayToTree2(items){
        const result = []
        const itemMap = {}
        for(const item of items){
            itemMap[item.id] = {...item,children:[]}
        }
        for(const item of items){
            const id = item.id
            const pid = item.pid
            const treeItem = itemMap[id]
            if(pid === 0){
                result.push(treeItem)
            }else{
                if(!itemMap[pid]){
                    itemMap[pid] = {
                        children:[]
                    }
                }
                itemMap[pid].children.push(treeItem)
            }
        }
        return result
    }
```

### map存储，一次遍历

```javascript
    arrayToTree3(items){
        const result = [];   // 存放结果集
        const itemMap = {};  // 
        for (const item of items) {
            const id = item.id;
            const pid = item.pid;
            if (!itemMap[id]) {
                itemMap[id] = {
                    children: [],
                }
            }
            itemMap[id] = {
                ...item,
                children: itemMap[id]['children']
            }
            const treeItem =  itemMap[id];

            if (pid === 0) {
                result.push(treeItem);
            } else {
                if (!itemMap[pid]) {
                    itemMap[pid] = {
                    children: [],
                    }
                }
                itemMap[pid].children.push(treeItem)
            }
        }
        return result;
    }
```