---
title: Day7学习笔记
date: 2021/07/26
---

## 前端设置请求头信息报错
因为项目需要，前端需要在请求头设置一个信息传给后端，开始定义的字段为Via，但是修改代码下发请求后F12报错
`Refused to set unsafe header "Via"`
查找了一下网上资料，原来w3c规定，当请求的header匹配以下不安全字符时，将被终止。
```
Accept-Charset
Accept-Encoding
Connection
Content-Length
Cookie
Cookie2
Content-Transfer-Encoding
Date
Expect
Host
Keep-Alive
Referer
TE
Trailer
Transfer-Encoding
Upgrade
User-Agent
Via
```

## CSS布局

##### 三列布局

经典的三列布局由左中右三列组成，其特点为连续两列宽度固定、剩余一列宽度自适应和三列高度固定且相等。以下以左中列宽度固定和右列宽度自适应为例，反之同理。整体的实现原理与上述两列布局一致

![三列布局](./image/三列布局.png)

```html
    <div class="three-column-layout">
        <div class="left"></div>
        <div class="center"></div>
        <div class="right"></div>
    </div>
```

overflow+float
```less
.three-column-layout{
    width: 400px;
    height: 400px;
    .left{
        float: left;
        width: 100px;
        height: 100%;
        background-color: #f66;
    }
    .center{
        float: left;
        width: 100px;
        height: 100%;
        background-color: #66f;
    }
    .right{
        overflow: hidden;
        height: 100%;
        background-color: #3c9;
    }
}
```

flex

```less
.three-column-layout{
    width: 400px;
    height: 400px;
    display: flex;
    .left{
        width: 100px;
        background-color: #f66;
    }
    .center{
        width: 100px;
        background-color: #66f;
    }
    .right{
        flex: 1;
        background-color: #3c9;
    }
}
```

##### 圣杯布局和双飞翼布局

圣杯布局和双飞翼布局都是由左中右三列组成，其特点为左右两列宽度固定、中间一列宽度自适应和三列高度固定且相等。

圣杯布局和双飞翼布局在大体相同下也存在一点不同，区别在于双飞翼布局中间列需插入一个子节点。在常规的实现方式中也是在这个中间列里做文章，如何使中间列内容不被左右列遮挡。

- 相同
  - 中间列放首位且声明其宽高占满父节点
  - 被挤出的左右列使用float和margin负值将其拉回与中间列处在同一水平线上

- 不同
  - 圣杯布局：父节点声明padding为左右列留出空位，将左右列固定在空位上
  - 双飞翼布局：中间列插入子节点并声明margin为左右列让出空位，将左右列固定在空位上
