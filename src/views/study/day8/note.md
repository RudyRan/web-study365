---
title: Day8学习笔记
date: 2021/07/27
---

## Vue 组件

### 组件的三个API

#### prop

- 写通用组件时，prop最好采用对象的写法，这样可以针对每个属性设置类型、默认值或者自定义校验属性的值。
- 组件不能自己修改props的值，只能通过父组件修改
```javascript
<template>
  <button :class="'i-button-size' + size" :disabled="disabled"></button>
</template>
<script>
  // 判断参数是否是其中之一
  function oneOf (value, validList) {
    for (let i = 0; i < validList.length; i++) {
      if (value === validList[i]) {
        return true;
      }
    }
    return false;
  }

  export default {
    props: {
      size: {
        validator (value) {
          return oneOf(value, ['small', 'large', 'default']);
        },
        default: 'default'
      },
      disabled: {
        type: Boolean,
        default: false
      }
    }
  }
</script>
```
请注意上面代码中的size校验部分，指定了从父组件传到子组件的值必须是small、large、default中的一个

#### 插槽slot

```html
<!-- 子组件 -->
<template>
  <button :class="'i-button-size' + size" :disabled="disabled">
    <slot></slot>
  </button>
</template>

<!-- 父组件 -->
<i-button>
  <strong>按钮 2</strong>
</i-button>
```


具名插槽

```html
<!-- 子组件 -->
<template>
  <button :class="'i-button-size' + size" :disabled="disabled">
    <slot name="icon"></slot>
    <slot></slot>
  </button>
</template>

<!-- 父组件 -->
<i-button>
  <i-icon slot="icon" type="checkmark"></i-icon>
  按钮 1
</i-button>
```
可以在插槽中写一些默认内容，这样在父组件没有写任何slot时，就显示默认内容

#### 自定义事件event

```javascript
<template>
  <button @click="handleClick">
    <slot></slot>
  </button>
</template>
<script>
  export default {
    methods: {
      handleClick (event) {
        this.$emit('on-click', event);
      }
    }
  }
</script>
```

通过`$emit`触发自定义事件`on-click`，在父组件上使用`on-click`监听

`<i-button @on-click="handleClick"></i-button>`

还有另外一种写法，使用.native修饰符，即
`<i-button @click.native="handleClick"></i-button>`
但是注意，这种写法已经在Vue3中被废弃了

### 组件的通信

![组件通信](./image/组件通信.png)

如上图所示，A 和 B、B 和 C、B 和 D 都是父子关系，C 和 D 是兄弟关系，A 和 C 是隔代关系（可能隔多代）。组件间经常会通信，Vue.js 内置的通信手段一般有两种

- `ref`：给元素或组件注册引用信息
- `$parent / $children`：访问父 / 子实例

举例$ref的使用
```javascript
// component-a
export default {
  data () {
    return {
      title: 'Vue.js'
    }
  },
  methods: {
    sayHello () {
      window.alert('Hello');
    }
  }
}

<template>
  <component-a ref="comA"></component-a>
</template>
<script>
  export default {
    mounted () {
      const comA = this.$refs.comA;
      console.log(comA.title);  // Vue.js
      comA.sayHello();  // 弹窗
    }
  }
</script>
```
上面两种方式的弊端是无法做到跨级别或者兄弟之间通信，当A组件和B组件之间隔了数代时，一般会借助Vuex或者Bus的解决方案

### provide / inject

```javascript
// A.vue
export default {
  provide: {
    name: 'Rudy'
  }
}

// B.vue
export default {
  inject: ['name'],
  mounted () {
    console.log(this.name);  // Rudy
  }
}
```
父组件通过provide方式提供了一个name，子组件通过inject注入变量然后访问

注意：provide 和 inject 绑定并不是可响应的。这是刻意为之的。然而，如果你传入了一个可监听的对象，那么其对象的属性还是可响应的

所以，上面 A.vue 的 name 如果改变了，B.vue 的 this.name 是不会改变的

### 替换Vuex

如何通过provide / inject方式模拟VueX的功能？
主要还是在App.vue上做文章

```javascript
<template>
  <div>
    <router-view></router-view>
  </div>
</template>
<script>
export default {
    provide () {
        return {
            app: this
        }
    }
}
</script>
```
把整个app.vue的实例this对外提供，命名为app，那么其他组件通过inject注入app的话，就可以通过this.app.xxx访问app.vue的data、computed、methods内容

```javascript
<script>
  export default {
    provide () {
      return {
        app: this
      }
    },
    data () {
      return {
        userInfo: null
      }
    },
    methods: {
      getUserInfo () {
        // 这里通过 ajax 获取用户信息后，赋值给 this.userInfo，以下为伪代码
        $.ajax('/user/info', (data) => {
          this.userInfo = data;
        });
      }
    },
    mounted () {
      this.getUserInfo();
    }
  }
</script>
```

这样其他组件通过Inject注入app后，就可以直接访问userInfo的数据，也可以访问app中的getUserInfo方法

如果项目太复杂，在app.vue中写入太多代码显然是不合适的，可以采用mixins把逻辑分到不同的js文件

### JS类型判断

基础数据类型：
Undefined、Null、Boolean、Number、String、Object、Symbol(ES6新增)

#### typeof

通过typeof判断得到的结果分别是
undefined、object、boolean、number、string、object、symbol

结论：对于基本数据类型，使用typeof 无法区别null和object

对于引用类型，比如Array、Function、Data、RegExp等，只能检测出Function类型

```javascript
function a() {}

console.log(typeof a); // function

```

#### Object.prototype.toString

 Object.prototype.toString 会返回一个由 "[object " 和 class 和 "]" 组成的字符串，而 class 是要判断的对象的内部属性

```javascript
var number = 1;          // [object Number]
var string = '123';      // [object String]
var boolean = true;      // [object Boolean]
var und = undefined;     // [object Undefined]
var nul = null;          // [object Null]
var obj = {a: 1}         // [object Object]
var array = [1, 2, 3];   // [object Array]
var date = new Date();   // [object Date]
var error = new Error(); // [object Error]
var reg = /a/g;          // [object RegExp]
var func = function a(){}; // [object Function]

console.log(Object.prototype.toString.call(Math)); // [object Math]
console.log(Object.prototype.toString.call(JSON)); // [object JSON]

function a() {
    console.log(Object.prototype.toString.call(arguments)); // [object Arguments]
}
a();

```

### type

既然已经知道了typeof和Object.prototype.toString两种判断JS类型的方式，那可以实现一个type函数，
如果是基础数据类型就是用typeof，否则使用Object.prototype.toString

第一版：

```javascript
// 第一版
var class2type = {};

// 生成class2type映射
"Boolean Number String Function Array Date RegExp Object Error Null Undefined".split(" ").map(function(item, index) {
    class2type["[object " + item + "]"] = item.toLowerCase();
})

function type(obj) {
    return typeof obj === "object" || typeof obj === "function" ?
        class2type[Object.prototype.toString.call(obj)] || "object" :
        typeof obj;
}

```

考虑兼容性，IE6中null和undefined会被 Object.prototype.toString 识别成 [object Object]
因此改写一下，如下：

```javascript

// 第一版
var class2type = {};

// 生成class2type映射
"Boolean Number String Function Array Date RegExp Object Error Null Undefined".split(" ").map(function(item, index) {
    class2type["[object " + item + "]"] = item.toLowerCase();
})

function type(obj) {
    if(obj == null){
      return obj + ''
    }
    return typeof obj === "object" || typeof obj === "function" ?
        class2type[Object.prototype.toString.call(obj)] || "object" :
        typeof obj;
}
```