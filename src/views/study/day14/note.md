## new

new 主要实现了两个功能
1. 访问构造函数里的属性
2. 访问原型的属性，即构造函数.prototype的属性

### 第一版

```javascript
    objectFactory(){
        let obj = new Object()
        // arguments是伪数组，不能直接用数组方法
        // shift删除数组的第一个元素并返回
        let Constructor = [].shift.call(arguments)
        // 将 obj 的原型指向构造函数，这样 obj 就可以访问到构造函数原型中的属性
        obj.__proto__ = Constructor.prototype
        //使用 apply，改变构造函数 this 的指向到新建的对象，这样 obj 就可以访问到构造函数中的属性
        Constructor.apply(obj,arguments)
        return obj
    }
```

### 第二版

构造函数如果有返回值怎么办？

比如：

```javascript
function Otaku (name, age) {
    this.strength = 60;
    this.age = age;

    return {
        name: name,
        habit: 'Games'
    }
}

var person = new Otaku('Kevin', '18');

console.log(person.name) // Kevin
console.log(person.habit) // Games
console.log(person.strength) // undefined
console.log(person.age) // undefined
```

可以看到在返回了一个对象后，在实例中只能访问到对象的值。
那返回基本类型呢？

```javascript
function Otaku (name, age) {
    this.strength = 60;
    this.age = age;

    return 'handsome boy';
}

var person = new Otaku('Kevin', '18');

console.log(person.name) // undefined
console.log(person.habit) // undefined
console.log(person.strength) // 60
console.log(person.age) // 18
```

结果相反，在实际的结果中不会有返回值的存在

那我们改写一下new的方法，如下：

