const HtmlWebpackPlugin = require('html-webpack-plugin');
<<<<<<< HEAD
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const isDev = process.env.NODE_ENV === 'development'
const path = require('path');
const config =require('./public/config')[isDev ? 'dev' : 'build']
module.exports = {
    mode: isDev ? 'development' : 'production',
    entry: './src/index.js', //webpack的默认配置
    output: {
        path: path.resolve(__dirname, 'dist'), //必须是绝对路径
        filename: 'bundle.[hash].js',
        publicPath: '/', //通常是CDN地址
        clean:true
    },
    // devtool: 'cheap-module-source-map', //开发环境下使用
=======
const isDev = process.env.NODE_ENV === 'development'
const config =require('./public/config')[isDev ? 'dev' : 'build']
module.exports = {
    mode: isDev ? 'development' : 'production',
    devtool: 'cheap-module-source-map', //开发环境下使用
>>>>>>> 1b4267e9118474ee22fc3f8bc1f91c12c8a7af7f
    module:{
        rules:[
            {
                test: /\.jsx?$/,
                use:['babel-loader'],
                // options:{
                //     "presets": ["@babel/preset-env"],
                //     "plugins": [
                //         [
                //             "@babel/plugin-transform-runtime",
                //             {
                //                 "corejs":3
                //             }
                //         ]
                //     ]
                // },
                exclude:/node_modules/ //排除node_modules目录
            },
            {
                test: /\.(le|c)ss$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'less-loader'],
                exclude: /node_modules/
<<<<<<< HEAD
            },
            {
                test: /\.(png|jpg|gif|jpeg|webp|svg|eot|ttf|woff|woff2)$/,
                use:[
                    {
                        loader:'url-loader',
                        options:{
                            limit:10240,
                            esModule:false,
                            name:'[path][name]_[hash:8].[ext]',
                            outputPath:'assets',
                        }
                    }
                ],
                exclude:/node_modules/ //排除node_modules目录
=======
>>>>>>> 1b4267e9118474ee22fc3f8bc1f91c12c8a7af7f
            }
        ]
    },
    plugins:[
        //数组 放着所有的webpack插件
        new HtmlWebpackPlugin({
            template: './public/index.html',
            filename: 'index.html', //打包后的文件名
            config: config.template,
            minify: {
                removeAttributeQuotes: false, //是否删除属性的双引号
                collapseWhitespace: false, //是否折叠空白
            },
            // hash: true //是否加上hash，默认是 false
<<<<<<< HEAD
        }),
        // new CleanWebpackPlugin() 
=======
        })
>>>>>>> 1b4267e9118474ee22fc3f8bc1f91c12c8a7af7f
    ],
    devServer:{
        port:'3000',
        stats:'errors-only', //终端只打印error
        clientLogLevel: "silent", //日志等级
        compress:true //是否开启gzip压缩
    }
}