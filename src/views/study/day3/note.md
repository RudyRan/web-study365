---
title: Day3学习笔记
date: 2021/07/20
---

## Linux命令

### 修改文件夹名
mv 旧文件夹名 新文件夹名

### 复制文件到另一个目录
cp 复制目录 目标目录

cp -r 复制目录 目标目录 -r 复制全部内容

## webpack学习

### 安装
npm install webpack webpack-cli -D

### babel-loader

#### 安装

`npm install babel-loader -D`

`npm install --save-dev @babel/core @babel/cli @babel/preset-env` //安装依赖

`npm install @babel/plugin-transform-runtime -D`

`npm install @babel/runtime-corejs3`

关于@babel/plugin-transform-runtime的作用，请参考
https://zhuanlan.zhihu.com/p/147083132

#### 配置babel

1.  babelrc方式
```json
{
    "presets": ["@babel/preset-env"],
    "plugins": [
        [
            "@babel/plugin-transform-runtime",
            {
                "corejs":3
            }
        ]
    ]
}
```

2.  webpack内部配置

```javascript

module.exports = {
    module:{
        rules:[
            {
                test: /\.jsx?$/,
                use:['babel-loader'],
                options:{
                    "presets": ["@babel/preset-env"],
                    "plugins": [
                        [
                            "@babel/plugin-transform-runtime",
                            {
                                "corejs":3
                            }
                        ]
                    ]
                },
                exclude:/node_modules/ //排除node_modules目录
            }
        ]
    }
}
```

总结：loader配置在moudle.rules中，是一个数组。基础格式为：
```javascript
{
    test: /\.jsx?$/,
    use:['babel-loader'],
    options:{
        ...
    }
}
```
其中use可以是一个字符串，也可以是一个数组，比如对css文件的处理```use: ['style-loader', 'css-loader']```

#### 配置Mode

```javascript
module.exports = {
    //....
    mode: "development",
    module: {
        //...
    }
}
```
mode配置项主要有：

- development:开发环境
- production:生产环境

#### 在浏览器查看页面

需要一个插件```html-webpack-plugin```,该插件主要有两个作用

- 为html文件中引入的外部资源如script、link动态添加每次compile后的hash，防止引用缓存的外部文件问题
- 可以生成创建html入口文件，比如单页面可以生成一个html文件入口，配置N个html-webpack-plugin可以生成N个页面入口

```javascript
    plugins:[
        //数组 放着所有的webpack插件
        new HtmlWebpackPlugin({
            template: './public/index.html',
            filename: 'index.html', //打包后的文件名
            minify: {
                removeAttributeQuotes: false, //是否删除属性的双引号
                collapseWhitespace: false, //是否折叠空白
            },
            // hash: true //是否加上hash，默认是 false
        })
    ]
```

灵活运用config参数
有时候可能需要根据不同的情况显示区加载不同的脚本文件或者对模板文件进行定制化处理，这是如果直接修改index.html是很麻烦的事，此时就可以通过配置一个config文件为不同的业务需求生成不同的html文件

1.  首先在public下新增config.js文件，文件配置如下

```javascript
module.exports = {
    dev: {
        template: {
            title: '你好',
            header: false,
            footer: false
        }
    },
    build: {
        template: {
            title: '你不好',
            header: true,
            footer: false
        }
    }
}

```
2.  修改webpack
   
```javascript
//webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin');
const isDev = process.env.NODE_ENV === 'development';
const config = require('./public/config')[isDev ? 'dev' : 'build'];

modue.exports = {
    //...
    mode: isDev ? 'development' : 'production'
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            filename: 'index.html', //打包后的文件名
            config: config.template
        })
    ]
}

```

3.  修改public/index.html文件

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <% if(htmlWebpackPlugin.options.config.header) { %>
    <link rel="stylesheet" type="text/css" href="//common/css/header.css">
    <% } %>
    <title><%= (htmlWebpackPlugin.options.config.title) %></title>
</head>

<body>
</body> 
<% if(htmlWebpackPlugin.options.config.header) { %>
<script src="//common/header.min.js" type="text/javascript"></script> 
<% } %>
</html>
```

4. 在script中添加命令
```javascript
{
    "scripts": {
        "dev": "cross-env NODE_ENV=development webpack",
        "build": "cross-env NODE_ENV=production webpack"
    }
}
注：cross-env是为了解决NODE_ENV=development设置，这种设置方式在windows上不支持，会提示将阻塞
```

5.  分别运行```npm run dev``` 和 ```npm run build```得到的结果如下

- ```npm run dev```
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>你好</title>
    <script defer="defer" src="main.js"></script></head>
<body>

</body>
</html>
```
- ```npm run build```
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="//common/css/header.css">
    <title>你不好</title>
<script defer="defer" src="main.js"></script></head>
<body>
    <script src="//common/header.min.js" type="text/javascript"></script> 
</body>
</html>
```

#### 在浏览器中查看效果
安装```npm install webpack-dev-server -D```
修改packjson中scripts部分

```javascript
"scripts": {
    "dev": "cross-env NODE_ENV=development webpack-dev-server",
    "build": "cross-env NODE_ENV=production webpack"
}
```
运行报错：Error: Cannot find module 'webpack-cli/bin/config-yargs'
原因是webpack-cli和webpack-dev-server不兼容导致
解决方式1：
修改script脚本为
```javascript
  "scripts": {
    "dev": "cross-env NODE_ENV=development webpack serve --config webpack.config.js --open",
    "build": "cross-env NODE_ENV=production webpack"
  },
```
解决方式2：
修改webpack-cli 4 安装webpack-cli 3

webpack-dev-server的一些配置
```javascript
    devServer:{
        port:'3000',
        stats:'errors-only', //终端只打印error
        clientLogLevel: "silent", //日志等级
        compress:true //是否开启gzip压缩
    }
```

当你修改index.js文件时，比如增加一个console语句，保存后查看控制台也会刷新，但是仔细查看发现console里输出语句的行号和代码中的不一致，点击进度查看发现代码是编译后的，这对我们开发调试很不方便，因此我们需要devtool这个工具

#### devtool
devtool可以将我们编译后的代码映射回原始源代码，参数主要有以下几种
1.  source-map 
产生单独的source-map文件，功能完全，但是会减慢打包速度

2.  eval-source-map
使用eval打包源文件模块，直接在源文件中写入干净完整的source-map，不影响构建速度，但影响执行速度和安全，建议开发环境中使用，生产阶段不要使用

3. cheap-module-source-map
会产生一个不带映射到列的单独的map文件，开发者工具就只能看到行

4. cheap-module-eval-source-map
不会产生单独的map文件，（与eval-source-map类似）但开发者工具就只能看到行，但无法对应到具体的列（符号），对调试不便

如果只需要能定位到对应的行，就设置
`devtool: 'cheap-module-source-map'`
在生产环境一般就使用`none`或者`source-map`

#### 处理CSS样式

##### 安装loader

`npm install style-loader less-loader css-loader postcss-loader autoprefixer less -D`

##### 配置loader

```javascript
    {
        test: /\.(le|c)ss$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'less-loader'],
        exclude: /node_modules/
    }
```
新建postcss.config.js配置文件，内容如下：
```javascript
    module.exports = {
        loader: 'postcss-loader',  
        plugins: {
            autoprefixer: {}
        }
    }
```
新建.browserslistrc文件，内容如下：
```
> 1%
last 2 versions
not ie <= 10
```

注：loader之间的执行顺序是从后向前执行的，因此上面的loader执行顺序是`less-loader`->`postcss-loader`->`css-loader`->`style-loader`

#### 处理图片&文字

##### 安装
`npm install url-loader file-loader -D`

##### 配置
```javascript
    {
        test: /\.(png|jpg|gif|jpeg|webp|svg|eot|ttf|woff|woff2)$/,
        use: [
            {
                loader: 'url-loader',
                options: {
                    limit: 10240, //10K
                    esModule: false ,
                    name:'[path][name]_[hash:8].[ext]',
                    outputPath:'assets', //指定输出目录
                }
            }
        ],
        exclude: /node_modules/
    }
```

#### 入口/出口配置

入口：entry

`entry: './src/index.js'` 
entry可以是一个字符串或者数组或者对象，数组时就表示多入口

出口：output

```javascript
    output: {
        path: path.resolve(__dirname, 'dist'), //必须是绝对路径
        filename: 'bundle.[hash].js',
        publicPath: '/' //通常是CDN地址
    }

```
[hash]主要是为了解决CDN缓存问题，也可以指定hash长度[hash:6]

#### 清空dist目录

安装插件`npm install clean-webpack-plugin -D`

```javascript
    plugins:[
        ...
        new CleanWebpackPlugin({
             cleanOnceBeforeBuildPatterns:['**/*', '!dll', '!dll/**'] //不删除dll目录下的文件
        }) 
    ],
```
希望dist下某个目录不清空  `cleanOnceBeforeBuildPatterns`

该插件在webpack5.20以上版本被移除，一般直接在output中设置clean属性实现

```javascript
output:{
    // clean:true
    clean:{
        keep: /ignored\/dir\//, // 保留部分文件目录
    }
}
```