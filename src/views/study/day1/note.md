---
title: Day1学习笔记
date: 2021/07/18
---

## Day1 学习笔记

### MarkDown语法

#### 任务列表

- [ ] 任务一 未做任务 `- + 空格 + [ ]`
- [x] 任务二 已做任务 `- + 空格 + [x]`

#### 字体效果

- 斜体 *斜体*

- 粗体 **粗体**

- 加粗斜体 ***加粗斜体***

- 删除线 ~~删除线~~

#### 链接方式

- 行内样式

    欢迎访问 [百度](https://www.baidu.com)

- 参考式

    我经常去的几个网站[Google][1]、[Leanote][2]。

[1]:http://www.google.com 
[2]:http://www.leanote.com

- 注脚式

    使用 Markdown[^1]可以效率的书写文档, 直接转换成 HTML[^2]。

[^1]:Markdown是一种纯文本标记语言

[^2]:Markdown是一种纯文本标记语言

#### 列表

- 无序列表项
- 无序列表项

1. 有序列表
2. 有序列表

#### 颜色&字号

<font color=#0099ff size=5 face="黑体">黑体</font>

#### 代码块

如何高效学习`javascript`

```javascript
const arr = [1,2,3]
```

#### 表格

|学号|姓名|序号|
|-|-|-|
|小明明|男|5|
|小红|女|79|
|小陆|男|192|

#### 分割线

***
