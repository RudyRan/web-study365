---
title: Day4学习笔记
date: 2021/07/21
---

## 前端缓存

分为强缓存和协商缓存，主要区别就是在使用缓存的时候是否需要向服务器验证本地缓存是否有效。

### 强缓存

如果缓存没过期，就取缓存资源，否则就去请求服务器

主要参数:查看response headers中的Cache-Control值

![avatar](./image/Image%201.png)

如上图所示：Cache-Control:max-age = 3600 就表示在3600s内，都是直接使用缓存，超过了就请求服务器
和Cache-Control并列使用的还有一个Expires,已经基本被淘汰


Cache-Control作为响应首部时，取值主要有：
-   private:仅浏览器可以缓存
-   public：浏览器和代理服务器都可以缓存
-   max-age=xxx 设置缓存过期时间
-   no-cache 不进行强缓存
-   no-store 不强缓存也不进行协商缓存

Cache-Control可以同时配置多个规则

对于强缓存而言，主要研究Cache-Control中的 max-age 和 no-cache, 如果存在max-age = xxx就命中强缓存
如果Cache-Caontrol是no-cache，就没命中强缓存，走协商缓存

强缓存步骤：
1.  第一次请求 a.js ，缓存表中没该信息，直接请求后端服务器。
2.  后端服务器返回了 a.js ，且 http response header 中 cache-control 为 max-age=xxxx，所以是强缓存规则，存入缓存表中
3.  第二次请求 a.js ，缓存表中是 max-age， 那么命中强缓存，然后判断是否过期，如果没过期，直接读缓存的a.js，如果过期了，则执行协商缓存的步骤

### 协商缓存

触发条件：
1.  Cache-Control的值为no-cache
2.  max-age过期
但是Cache-Control的值不能为no-store

协商缓存主要是通过以下几个字段来实现缓存功能的。

#### Last-Modified

服务器将资源传递给客户端时，会将资源的最后更改时间通过Last-Modified字段放到响应体里返回给客户端。
客户端在下次请求时，会把服务端Last-Modified的内容一并带给服务端取进行检查，如果传递的时间值和服务器上的资源修改时间是一致的，
则说明资源未被修改，直接返回304状态码，如果时间不一致，服务端返回200并发送新的资源给客户端。

客户端向服务端传递修改时间的请求报文首部字段为：
- If-Modified-Since
示例： If-Modified-Since: Thu, 31 Mar 2016 07:07:52 GMT
该请求首部告诉服务器如果客户端传来的最后修改时间与服务器上的一致，则直接回送304 和响应报头即可。
当前各浏览器均是使用的该请求首部来向服务器传递保存的 Last-Modified 值。

如果在 If-Modified-Since字段指定时间后，服务端资源发生了更新，服务器则会接受请求

- If-Unmodified-Since:

和If-Modified-Since含义相反
只有当资源在指定的时间之后没有进行过修改的情况下，服务器才会返回请求的资源,否则返回412错误

场景：例如在某些wiki应用中的做法：假如在原始副本获取之后，服务器上所存储的文档已经被修改，那么对其作出的编辑会被拒绝提交

注：Last-Modifed的最小单位为秒，如果资源在1s内被多次修改，则该方法失效

#### ETag
Etag：唯一标识符，服务器通过某种算法给资源计算出一个表示符并在将资源响应给客户端的时候在响应首部加上Etag
客户端在收到请求后会保留该字段，在下一次请求时一起带给服务端，服务端通过比较两次的Etag值是否相等来判断字段是否被修改过，是让客户端走缓存还是
重新发送资源。

客户端标记Etag值主要是通过以下两个字段：

- If-None-Match

示例： If-None-Match: "5d8c72a5edda8d6a:3239"
客户端比较Etag值和If-None-Match值，相同则返回304，否则重新发送数据

- If-Match
只有当If-Match字段和Etag字段一致时，服务端才会接受请求并相应数据，否则返回412



缺点：计算Etag值存在性能损耗，分布式服务器存储的情况下，计算ETag的算法如果不一样，会导致浏览器从一台服务器上获得页面内容后到另外一台服务器上进行验证时发现ETag不匹配的情况



### 用户刷新/访问行为

用户访问界面手段主要分为三类

- 在URI输入栏中输入然后回车/通过书签访问
- F5/点击工具栏中的刷新按钮/右键菜单重新加载
- Ctl+F5

<https://imweb.io/topic/5795dcb6fb312541492eda8c>