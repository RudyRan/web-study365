
## 类数组对象

类数组对象不能直接使用数组对象的方法
必须通过Function.call间接调用

```javascript
    Array.prototype.join.call(arrayLike, '&'); // name&age&sex

    Array.prototype.slice.call(arrayLike, 0); // ["name", "age", "sex"] 
    // slice可以做到类数组转数组

    Array.prototype.map.call(arrayLike, function(item){
        return item.toUpperCase();
    }); 
```

### 类数组转数组

```javascript
    var arrayLike = {0: 'name', 1: 'age', 2: 'sex', length: 3 }
    // 1. slice
    Array.prototype.slice.call(arrayLike); // ["name", "age", "sex"] 
    // 2. splice
    Array.prototype.splice.call(arrayLike, 0); // ["name", "age", "sex"] 
    // 3. ES6 Array.from
    Array.from(arrayLike); // ["name", "age", "sex"] 
    // 4. apply
    Array.prototype.concat.apply([], arrayLike)
```

### arguments对象

arguments对象的length属性，表示实参的长度

arguments对象的callee属性，通过它可以调用函数自身

## JavaScript创建对象的多种方式以及优缺点





## 继承方式

### 原型链继承
```javascript
    // 原型链继承
    function Parent(){
        this.name = 'rudy'
    }
    Parent.prototype.getName = function(){
        console.log(this.name)
    }
    function Child(){

    }
    Child.prototype = new Parent()
    let child = new Child()
    child.getName()
```

问题：引用类型的属性会被所有实例共享 不能向Parent传参

```javascript
    function Parent(){
        this.names = ['rudy','ran']
    }
    Parent.prototype.getName = function(){
        console.log(this.names)
    }
    function Child(){

    }
    Child.prototype = new Parent()
    var child1 = new Child();

    child1.names.push('yayu');

    console.log(child1.names); // ["kevin", "daisy", "yayu"]

    var child2 = new Child();

    console.log(child2.names); // ["kevin", "daisy", "yayu"]
    
```

### 借用构造函数继承

```javascript
    function Parent(){
        this.names = ['rudy','ran']

    }
    function Child(name){
      Parent.call(this,name)
    }
    var child = new Child()
    child.names.push('test')
    console.log(child.names)

    var child2 = new Child()
    console.log(child2.names)
```

优点

- 避免引用类型属性被共享
- 可以在Child中向Parent传参

缺点

方法都在构造函数中定义，每次创建实例都会创建一遍方法


### 组合继承

```javascript
function Parent (name) {
    this.name = name;
    this.colors = ['red', 'blue', 'green'];
}

Parent.prototype.getName = function () {
    console.log(this.name)
}

function Child (name, age) {

    Parent.call(this, name);
    
    this.age = age;

}

Child.prototype = new Parent();
Child.prototype.constructor = Child;

var child1 = new Child('kevin', '18');

child1.colors.push('black');

console.log(child1.name); // kevin
console.log(child1.age); // 18
console.log(child1.colors); // ["red", "blue", "green", "black"]

var child2 = new Child('daisy', '20');

console.log(child2.name); // daisy
console.log(child2.age); // 20
console.log(child2.colors); // ["red", "blue", "green"]
```

## vue组件通信

- props/$emit
- $children/$parent
- provide/reject
- ref
- eventBus
- Vuex
- localStorage/sessionStorage
- $attrs/$listeners

### props/$emit

 prop 只可以从上一级组件传递到下一级组件（父子组件），即所谓的单向数据流。而且 prop 只读，不可被修改，所有修改都会失效并警告

 ```javascript
//  父组件
 <template>
  <div class="section">
      <child :articles="articleList"/>
  </div>
</template>

<script>
import Child from './Child.vue'
export default {
    name:'Parent',
    components:{
        Child
    },
    data(){
        return {
              articleList: ['红楼梦', '西游记', '三国演义']
        }
    }
}

// 子组件
<template>
  <div>
      <span v-for="(item,index) in articles" :key="index">
          {{item}}
      </span>
  </div>
</template>

<script>
export default {
    name:'Child',
    props:{
        articles:{
            type:Array
        }
    }
}
</script>
</script>
 ```


 