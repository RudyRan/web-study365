---
title: Day2学习笔记
date: 2021/07/19
---

## 数组扁平化

数组扁平化就是将一个多层嵌套的数组转化成只有一层的数组

### 递归实现

```javascript
    flatten(arr){
        let result = []
        for(let i=0,len=arr.length;i<len;i++){
            if(Array.isArray(arr[i])){
                result = result.concat(this.flatten(arr[i]))
            }else{
                result.push(arr[i])
            }
        }
        return result
    },
```

### toString方式

这种方式只适用于数字类型的数组

```javascript
    flatten(arr){
        return arr.toString().split(',').map(function (item) {
            return parseInt(item)
            // return +item
        })
    }
```

### reduce方式实现

```javascript
    flatten(arr){
        return arr.reduce((prev,next)=>{
            return prev.concat(Array.isArray(next) ? this.flatten(next): next)
        },[])
    }
```

### ES6扩展运算符

```javascript
    flatten(arr){
        while(arr.some(item=>Array.isArray(item))){
            arr = [].concat(...arr)
        }
        return arr
    }
```

### 最终实现（参考underscore）

```javascript
    /**
        * 数组扁平化
        * @param  {Array} input   要处理的数组
        * @param  {boolean} shallow 是否只扁平一层
        * @param  {boolean} strict  是否严格处理元素，下面有解释
        * @param  {Array} output  这是为了方便递归而传递的参数
    */
    flatten(input,shallow,strict,output){
        output = output || []
        let idx = output.length
        for(let i=0,len=input.length;i<len;i++){
            let value = input[i]
            if(Array.isArray(value)){
                if(shallow){
                    let j = 0,length = value.length;
                    while(j<length){
                        output[idx++] = value[j++]
                    }
                }else{
                    this.flatten(value,shallow,strict,output)
                    idx = output.length
                }
            }else if(!strict){
                output[idx++] = value
            }
        }
        return output
    }
```

