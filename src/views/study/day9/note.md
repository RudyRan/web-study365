---
title: Day9学习笔记
date: 2021/07/31
---

## Vue面试相关

### v-show和v-if的区别



1.  v-show只是CSS级别display:none和display:block之间的切换，而v-if决定是否选择代码块的内容（都为false v-if内容不会渲染，而v-show会渲染，只是被隐藏）
2.  频繁操作时，使用v-show，一次性渲染完的使用v-if

使用场景：

**v-if**
- 某一块代码在运行时很少改变
- 与 v-for 结合使用时，v-for 比 v-if 的优先级更高
- 和template配合使用，可以分组渲染代码块
- 与keep-alive结合使用可保留组件状态,避免重新渲染

**v-show**

- 需要频繁操作，使用v-show渲染
- 不可用于组件
- 没有条件语句

### 使用v-if在性能优化上有什么经验

当使用v-if=“false”时，内部组件是不会渲染的，所以在特定条件下才渲染部分组件时，可以先将条件设置成false
需要时（比如异步、比如$nextTick）在设置为true，这样可以优选渲染重要的其他内容

### 动态绑定class

最简单的方式：
`:class="{ 'active': isActive }"`
也可以把变量放到data里

```javascript
:class="classObj"
data(){
    return {
        classObj:{'active': true }
    }
}
```

也可以通过计算属性方式

```javascript
computed: {
  classObj: function () {
    return {
      active: this.isActive
    }
  }
}
```

数组方式
```javascript
:class="[isActive,isSort]"
data(){
    return{
        isActive:'active',
        isSort:'sort'
    }
}
```

数组和对象结合使用
`:class="[{ active: isActive }, 'sort']"`

### 计算属性和watch区别

计算属性：自动监听依赖值的变化，从而动态返回内容，监听是一个过程，在监听的值变化时，可以触发一个回调，并做一些事情
只是需要动态值，那就用计算属性；需要知道值的改变后执行业务逻辑，才用 watch

1. computed是一个对象时有哪些属性
get和set，默认只有get，set设置属性并不是直接修改计算属性，而是修改它的依赖

```javascript
computed: {
  fullName: {
    // getter
    get: function () {
      return this.firstName + ' ' + this.lastName
    },
    // setter
    set: function (newValue) {
      //this.fullName = newValue 这种写法会报错
      var names = newValue.split(' ')
      this.firstName = names[0]//对它的依赖进行赋值
      this.lastName = names[names.length - 1]
    }
  }
}

```

2. computed 和 methods 有什么区别

methods 是一个方法，它可以接受参数，而 computed 不能；computed 是可以缓存的，methods 不会；一般在 v-for 里，需要根据当前项动态绑定值时，只能用 methods 而不能用 computed，因为 computed 不能传参

3. watch 是一个对象时，它有哪些选项

- deep 是否深度监听
- handler 执行的函数
- immediate watch创立后是否立即执行,可以用来简化代码，比如需要在created里先获取列表这种
