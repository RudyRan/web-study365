
## 闭包

闭包是指那些能够访问自由变量的函数

自由变量:指在函数中使用的，但既不是函数的参数也不是函数的局部变量的变量

闭包 = 函数 + 函数能否访问自由变量

```javascript
var a = 1;

function foo() {
    console.log(a);
}

foo();
```
ECMAScript中，闭包指的是:
1. 从理论角度：所有的函数。
2. 从实践的角度：
   - 即使创建它的上下文已经销毁，它仍然存在（比如，内部函数从父函数中返回）
   - 在代码中引用了自由变量

```javascript
var scope = "global scope";
function checkscope(){
    var scope = "local scope";
    function f(){
        return scope;
    }
    return f;
}

var foo = checkscope();
foo();
```
分析下上面代码的执行上下文的变化，如下：
1. 进入全局代码，创建全局执行上下文，全局执行上下文压入执行上下文栈
2. 全局执行上下文初始化
3. 执行 checkscope 函数，创建 checkscope 函数执行上下文，checkscope 执行上下文被压入执行上下文栈
4. checkscope 执行上下文初始化，创建变量对象、作用域链、this等
5. checkscope 函数执行完毕，checkscope 执行上下文从执行上下文栈中弹出
6. 执行 f 函数，创建 f 函数执行上下文，f 执行上下文被压入执行上下文栈
7. f 执行上下文初始化，创建变量对象、作用域链、this等
8. f 函数执行完毕，f 函数上下文从执行上下文栈中弹出

可以看到在f执行的时候，checkscope函数已经被销毁了（从执行上下文栈中弹出）

我们知道f在执行上下文的时候会维护一个作用域链

```javascript
fContext = {
    Scope: [AO, checkscopeContext.AO, globalContext.VO],
}
```
就是因为这个作用域链，f函数在checkscope函数销毁后依然可以读到checkscopeContext.AO的值

## 必刷题

```javascript
var data = [];

for (var i = 0; i < 3; i++) {
  data[i] = function () {
    console.log(i);
  };
}

data[0]();
data[1]();
data[2]();
```
答案都是3

分析：在执行到data[0]()之前，此时的全局上下文VO为：

```javascript
globalContext = {
    VO:{
        data:[...],
        i:3
    }
}
```

当执行data[0]()的时候，data[0]的作用域链为：

```javascript
data[0]Context = {
    Scope:[AO,globalContext.VO]
}
```
data[0]Context的AO并没有i值，所以会查找globalContext.VO ，i为3，所以结果是3

改成闭包版本：

```javascript
var data = [];

for (var i = 0; i < 3; i++) {
  data[i] = (function (i) {
        return function(){
            console.log(i);
        }
  })(i);
}

data[0]();
data[1]();
data[2]();
```
结果是 0 1 2

分析：在执行到data[0]()之前，全局上下文

```javascript
globalContext = {
    VO:{
        data:[...],
        i:3
    }
}
```
跟之前一样，执行到data[0]函数的时候，data[0]函数的作用域链发生了变化

```javascript
data[0]Context = {
    Scope: [AO, 匿名函数Context.AO globalContext.VO]
}
```
匿名函数的执行上下文AO为：

```javascript
匿名函数Context = {
    AO: {
        arguments: {
            0: 0,
            length: 1
        },
        i: 0
    }
}
```
data[0]Context的AO没有i的值，就会沿着作用域链从匿名函数的Context.AO中查找，i此时为0

## call和apply的实现

call() 方法在使用一个指定的 this 值和若干个指定的参数值的前提下调用某个函数或方法

比如：

```javascript
var foo = {
    value: 1
};

function bar() {
    console.log(this.value);
}

bar.call(foo); // 1
```

1. call改变了this的指向，指向了foo
2. bar函数执行了

### 模拟实现第一步

思路：

当我们调用call的时候，改造foo对象如下：

```javascript
var foo = {
    value: 1,
    bar: function() {
        console.log(this.value)
    }
};

foo.bar(); // 1
```

这个时候this就指向了foo，所以模拟步骤主要是：

1. 将函数设为对象的属性
2. 执行该函数
3. 删除该函数

```javascript
Function.prototype.call2 = function(context){
    // 首先要获取调用call的函数，用this可以获取 因为this指向函数的调用
    context.fn = this
    context.fn()
    delete context.fn
}
```

```javascript
// 测试一下
var foo = {
    value: 1
};

function bar() {
    console.log(this.value);
}

bar.call2(foo); // 1
```
测试一下,结果是1

### 模拟实现第二步

call函数还能给定参数，参数不确定，我们可以从arguments对象中获取

```javascript
// 以上个例子为例，此时的arguments为：
// arguments = {
//      0: foo,
//      1: 'kevin',
//      2: 18,
//      length: 3
// }
// 因为arguments是类数组对象，所以可以用for循环
var args = [];
for(var i = 1, len = arguments.length; i < len; i++) {
    args.push('arguments[' + i + ']');
}

// 执行后 args为 ["arguments[1]", "arguments[2]", "arguments[3]"]
```
接下来需要把参数数组放到要执行函数里去，这里采用eval方法拼成一个函数

`eval('context.fn(' + args +')')`

所以第二版代码结果

```javascript
Function.prototype.call3 = function (context) {
    context.fn = this
    var args = []
    for(var i=1,len = arguments.length;i<len;i++){
        args.push('arguments['+i+']')
    }
    eval('context.fn('+args+')')
    delete context.fn
}
```

第三版

注意几个小问题：
1. this可以传null，当为null的时候，视为指向window

```javascript
var value = 1;

function bar() {
    console.log(this.value);
}

bar.call(null); // 1
```
2. 函数可以有返回值
   
```javascript
var obj = {
    value: 1
}

function bar(name, age) {
    return {
        value: this.value,
        name: name,
        age: age
    }
}

console.log(bar.call(obj, 'kevin', 18));
// Object {
//    value: 1,
//    name: 'kevin',
//    age: 18
// }
```

来看最终代码

```javascript
// 第三版
Function.prototype.call2 = function (context) {
    var context = context || window;
    context.fn = this;

    var args = [];
    for(var i = 1, len = arguments.length; i < len; i++) {
        args.push('arguments[' + i + ']');
    }

    var result = eval('context.fn(' + args +')');

    delete context.fn
    return result;
}

// 测试一下
var value = 2;

var obj = {
    value: 1
}

function bar(name, age) {
    console.log(this.value);
    return {
        value: this.value,
        name: name,
        age: age
    }
}

bar.call2(null); // 2

console.log(bar.call2(obj, 'kevin', 18));
// 1
// Object {
//    value: 1,
//    name: 'kevin',
//    age: 18
// }
```

### apply的实现

```javascript
Function.prototype.apply1 = function(context,arr){
    var context = context || window;
    context.fn = this
    var result
    if(!arr){
        result = context.fn()
    }else{
        for (var i = 0, len = arr.length; i < len; i++) {
            args.push('arr[' + i + ']');
        }
        result = eval('context.fn(' + args + ')') 
    }
    delete context.fn
    return result

}
```