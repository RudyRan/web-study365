---
title: Day6学习笔记
date: 2021/07/25
---

## HTML

### html语义化

选择合适的标签便于开发者阅读和写出优雅的代码，同时有利于浏览器爬虫的解析。
语义化优点：易于修改和维护，支持无障碍阅读，有利于seo，面向未来的html，浏览器可能提供更丰富的支持

- 尽可能少用无语义的标签div和span
- 在语义不明显，可以使用div和p时，尽量使用p，因为p默认情况有上下边距，对兼容特殊终端有利
- 不要使用纯样式标签，比如b、font、u等，改用css设置
- 需要强调的文本，可以包含在strong或者em标签中（浏览器预设样式，能用CSS指定就不用他们），strong默认样式是加粗（不要用b），em是斜体（不用i）
- 使用表格时，标题要用caption，表头用thead，主体部分用tbody包围，尾部用tfoot包围。表头和一般单元格要区分开，表头用th，单元格用td
- 表单域要用fieldset标签包起来，并用legend标签说明表单的用途
- 每个input标签对应的说明文本都需要使用label标签，并且通过为input设置id属性，在lable标签中设置for=someld来让说明文本和相对应的input关联起来

### seo优化

1. 合理的设计title、description和keywords

`<title>` ：只强调重点即可，尽量把重要的关键词放在前面，关键词不要重复出现，尽量做到每个页面的`<title>`标题中不要设置相同的内容

`<meta name="description">`：网页描述，需要高度概括网页内容，切记不能太长，过分堆砌关键词，每个页面也要有所不同

`<meta name="keywords">`：页面关键字，列举出几个页面的重要关键字即可，切记过分堆砌

2. 按照语义书写html代码

3. 页内链接，要加 “title” 属性加以说明，让访客和 “蜘蛛” 知道。而外部链接，链接到其他网站的，则需要加上 el="nofollow" 属性, 告诉 “蜘蛛” 不要爬，因为一旦“蜘蛛”爬了外部链接之后，就不会再回来了。

4. 不要乱用H标签，一个页面应该最多只有一个H1标题

5. 使用alt属性添加说明
`<img src="cat.jpg" width="300" height="200" alt="猫"  />`

6. 减少使用iframe框架，爬虫一般不会读取里面的内容

7. 重要内容不要放到JS中，爬虫不会读取JS中的内容

8. 谨慎使用`display:none`，所有引擎会过滤掉其中的大部分内容

### h5新增属性和标签

- 结构标签（语义化标签）

1. section。section标签定义文档中的节（section、区段）。比如章节、页眉、页脚或文档中的其他部分。
2. article。article标签装载显示一个独立的文章内容。还可以嵌套，则内层的artilce对外层的article标签有隶属的关系。例如，一个博客文章，可以用article显示，然后一 些评论可以以article的形式嵌入其中。
3. aside。标签内容之外与标签内容相关的辅助信息；
4. header。header标签定义文档的页面组合，通常是一些引导和导航信息。
5. hgroup。hgroup标签用于对网页或区段的标题元素（h1-h6）进行组合。例如，在一个区段中你有连续的h系列的标签元素，则可以用hgroup将他们括起来。
6. footer。页尾信息；
7. nav。nav标签定义显示导航链接。
8. figure。独立的单元，例如某个有图片与内容的新闻块。
9. time。time标签定义公历的时间（24 小时制）或日期，时间和时区偏移是可选的。该元素能够以机器可读的方式对日期和时间进行编码，这样， 举例说，用户代理能够把生日提醒或排定的事件添加到用户日程表中，搜索引擎也能够生成更智能的搜索结果。
10. mark。mark标签定义带有记号的文本。请在需要突出显示文本时使用 `<mark>` 标签。
11. figure。figure标签规定独立的流内容（图像、图表、照片、代码等等）。figure 元素的内容应该与主内容相关，但如果被删除，则不应对文档流产生影响。
12. figcaption。figcaption 标签定义 figure 元素的标题（caption）。"figcaption" 元素应该被置于 "figure" 元素的第一个或最后一个子元素的位置。
13. contextmenu。contextmenu 标签添加到系统右键菜单 [貌似这个功能只有firefox支持]

- 表单标签

1. email。必须输入邮件。
2. url。必须输入url地址。
3. number。必须输入数值。
4. range。必须输入一定范围内的数值。
5. Date Pickers。日期选择器。
6. search。搜索常规的文本域。
7. color。颜色。

- 媒体标签

1. video。视频
2. audio。音频
3. embed。嵌入内容（包括各种媒体），Midi、Wav、AU、MP3、Flash、AIFF等。
4. source。source设置两份或多份源文件的音频播放器。浏览器应该选择它所支持的文件（如果有的话）
5. track。诸如 video 元素之类的媒介规定外部文本轨道。用于规定字幕文件或其他包含文本的文件，当媒介播放时，这些文件是可见的。

- 其他标签

1. details。details标签用于描述文档或文档某个部分的细节。与 `<summary>` 标签 配合使用可以为 details 定义标题。标题是可见的，用户点击标题时，会显示出 details。
2. summary。summary标签包含 details 元素的标题，"details" 元素用于描述有关文档或文档片段的详细信息。
3. bdi。把用户名从周围的文本方向设置中隔离出来
4. canvas。canvas标签定义图形，比如图表和其他图像。只是图形容器，您必须使用脚本来绘制图形。
5. command。可以定义命令按钮，比如单选按钮、复选框或按钮。只有 Internet Explorer 9支持
6. datalist。比select更牛。用户不但可以选择还可以自己输入。
7. embed。embed标签定义嵌入的内容，比如插件。例：`<embed src="helloworld.swf" />`
8. keygen。keygen标签规定用于表单的密钥对生成器字段。当提交表单时，私钥存储在本地，公钥发送到服务器。
9. meter。meter标签定义度量衡（一般用于进度条）。仅用于已知最大和最小值的度量。例：`<meter value="3" min="0" max="10">3/10</meter><br><meter value="0.6">60%</meter>`
10. output。执行计算然后在 `<output>`元素中显示结果
11. progress。progress标签定义运行中的进度（进程）。例：`<progress value="22" max="100"></progress>`

### h5新增api

1. 本地化存储
localStorage、sessionStorage
2. webworker
3. history对象

## css

### 选择器和权重

- 10000：！important

- 1000:：内联 外联

- 100：ID选择器

- 10：类选择、伪类、属性选择器

- 1：标签选择器、伪元素选择器

- 0：通配符选择器、后代选择器、兄弟选择器

!important > 内联样式 = 外联样式 > ID选择器 > 类选择器 = 伪类选择器 = 属性选择器 > 标签选择器 = 伪元素选择器 > 通配选择器 = 后代选择器 = 兄弟选择器

### BFC

块级格式化上下文，它是指页面中一块独立的渲染区域，这块区域有自己独立的渲染规则，规则取决于这块区域的Box属性。

#### 创建块级格式化上下文

- 根元素
- 浮动元素（元素的 float 不是 none）
- 绝对定位元素（元素的 position 为 absolute 或 fixed）
- overflow 值不为 visible 的块元素 
- 被定义成块级的非块级节点：display:inline-block/table-cell/table-caption/flex/inline-flex/grid/inline-grid

#### BFC渲染规则

- 节点在垂直方向上按顺序排
- 节点垂直方向距离由margin决定，相邻节点的margin会发生重叠，以最大margin为合并值
- BFC是一个隔离且不受外界影响的独立容器列
- BFC不会与同级浮动区域重叠
- 计算BFC高度时其浮动子节点也参与计算

#### 使用场景

##### 清除浮动

**HTML**

```html
<div class="parents">
  <div class="child"></div>   
</div>
```

**CSS**

```css
.parents{
  width: 300px;
  border: 5px solid #EF6C72;
}
.child{
  width:100px;
  height: 100px;
  background-color: black;
  float: left;
}
```

![image](https://upload-images.jianshu.io/upload_images/13538569-42e445ef049300f9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

可以看到，父元素里面包含了一个子元素，在子元素浮动的情况下，脱离文档流，父元素高度不会被子元素撑开，这是如果我们需要让父元素被浮动的子元素撑开，根据BFC布局规则 **计算BFC的高度时，浮动元素也参与计算**，我们让父级元素生成一个BFC，则父元素内的子元素也会计算高度
可以通过以下方式生成一个BFC：

```css
.parents:{
  /*overflow: hidden;*/
  /*float: left;*/
  /*display: inline-block;*/
  /*position: absolute;*/
  }`
```

![image](https://upload-images.jianshu.io/upload_images/13538569-862d902d1c5f5c23.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

具体请参照上面BFC的创建。

##### 解决外边距塌陷

**HTML**

```html
<div class="div"></div>
<div class="div"></div>
```

**CSS**

```css
.div{
  border: 1px solid red;
  width: 50px;
  height: 50px;
  margin: 50px 0;
  background-color: pink;
}
```

页面显示如下
<!-- 父元素里面包了一个子元素，子元素浮动-->
![image](https://upload-images.jianshu.io/upload_images/13538569-9a94cccbd85fb287.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

所谓的外边距塌陷是指：块级元素的上下外边距有时候会合并，合并后的外边距等于合并前两个外边距中的较大值，这种现象称为外边距塌陷
解决外边距塌陷，我们可以根据 **Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠** 因此，我们可以让两个p标签属于不同的BFC，则他们之间就不会发生外边距的塌陷问题
方式如下：
**HTML**

```html
<div class="div"></div>
<div class="wrap">
  <div class="div"></div>
</div>
```

**CSS**

```css
.wrap{
  overflow: hidden;
}
```

效果如下：
![](https://upload-images.jianshu.io/upload_images/13538569-57b6869e8bb760dc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

### 盒模型

1. 标准盒模型
content-box:元素的width和height属性只包含content
2. IE盒模型
border-box:元素的width和height属性包含border+padding+content

设置盒模型通过`box-sizing`属性

### 布局方式

- 普通布局：`display:block/inline`
- 浮动布局：`float:left/right`
- 定位布局：`position:relative/absolute/fixed、left/right/top/bottom/z-index`
- 表格布局：`table`各种属性
- 弹性布局：`display:flex/inline-flex、flex各种属性`(重点)
- 多列布局：`columns`各种属性
- 栅格布局：`display:grid/inline-grid`
- 响应式布局：`em/rem/vw/vh/vmin/vmax、媒体查询`
  
#### 全屏布局

经典的全屏布局由顶部、底部、主体三部分组成，其特点为三部分左右满屏拉伸、顶部底部高度固定和主体高度自适应，主要应用在主体布局。该布局很常见，也是大部分Web应用主体的主流布局。通常使用`<header>`、`<footer>`和`<main>`三个标签语义化排版，`<main>`内还可插入`<aside>`作为侧栏

![全屏布局](./image/image1.png)

```html
  <div class="fullscreen-layout">
      <header></header>
      <main></main>
      <footer></footer>
  </div>
```

##### position + left/right/top/bottom方式

顶部、底部和主体声明`left:0`和`right:0`将其左右部分满屏拉伸；顶部和底部声明`top:0`和`bottom:0`分别将其吸顶和吸底，并声明俩高度为固定值；将主体的`top`和`bottom`分别声明为顶部高度和底部高度

```less
.fullscreen-layout{
    position: relative;
    width: 400px;
    height: 400px;
    header,main,footer{
        position: absolute;
        left: 0;
        right: 0;
    }
    header{
        top: 0;
        height: 50px;
        background-color: #f66;
    }
    footer{
        bottom: 0;
        height: 50px;
        background-color: #66f;
    }
    main{
        background-color: #3c9;
        top: 50px;
        bottom: 50px;
    }
}
```

##### flex

```less
.fullscreen-layout{
    display: flex;
    flex-direction: column;
    width: 400px;
    height: 400px;
    header{
        height: 50px;
        background-color: #f66;
    }
    footer{
        height: 50px;
        background-color: #66f;
    }
    main{
        flex: 1;
        background-color: #3c9;
    }
}
```

#### 多列布局

经典的两列布局由左右两列组成，其特点为一列宽度固定、另一列宽度自适应和两列高度固定且相等

##### 两列布局 

![两列布局](./image/image2.png)

```html
<div class="two-column-layout">
    <div class="left"></div>
    <div class="right"></div>
</div>
```

`float + margin-left/right`

```less
.two-column-layout{
    width: 400px;
    height: 400px;
    .left{
        float: left;
        background-color: #f66;
        width: 100px;
        height: 100%;
    }
    .right{
        height: 100%;
        background-color: #66f;
        margin-left: 100px;
    }
}
```

`overflow+float`

左列声明同上，右列声明overflow:hidden使其形成BFC区域与外界隔离

```less
.two-column-layout {
    width: 400px;
    height: 400px;
    .left {
        float: left;
        width: 100px;
        height: 100%;
        background-color: #f66;
    }
    .right {
        overflow: hidden;
        height: 100%;
        background-color: #66f;
    }
}
```

`flex`

```less
.two-column-layout{
    display: flex;
    width: 400px;
    height: 400px;
    .left{
        width: 100px;
        background-color: #f66;
    }
    .right{
        flex: 1;
        background-color: #66f;
    }
}
```


